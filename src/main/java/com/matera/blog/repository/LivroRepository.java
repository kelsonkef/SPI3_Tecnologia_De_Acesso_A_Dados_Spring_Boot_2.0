package com.matera.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.matera.blog.model.Livro;

public interface LivroRepository extends JpaRepository<Livro, Long> {

}
