package com.matera.blog.restTeste;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.matera.blog.model.Livro;
import com.matera.blog.repository.LivroRepository;
import com.matera.blog.service.LivroService;

@RestController
public class LivroRest {
	
	@Autowired
    private LivroRepository instanceRepository;

    public  LivroRest(LivroRepository repository) {
        this.instanceRepository = repository;
    }

    @RequestMapping(value = "/restLivro", method = RequestMethod.GET)
    @ResponseBody
    public List<Livro> findAll() {
        return this.instanceRepository.findAll();
    }	
}
