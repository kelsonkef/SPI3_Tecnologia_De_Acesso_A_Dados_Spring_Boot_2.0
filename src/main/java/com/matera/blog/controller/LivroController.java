package com.matera.blog.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.matera.blog.model.Livro;
import com.matera.blog.service.LivroService;

@Controller //Define a classe como um bean do Spring
public class LivroController {
	
	@Autowired
	private LivroService service; //	Injeta a classe de serviços
	
	//Vai para tela principal do CRUD aonde são listados todos os livros
	@GetMapping("/livro")
	public ModelAndView findAll() {
		
		ModelAndView mv = new ModelAndView("/livro");
		mv.addObject("livros", service.findAll());
		
		return mv;
	}
	
	//Vai para tela de adição de livro
	@GetMapping("/livro/add")
	public ModelAndView add(Livro livro) {
		
		ModelAndView mv = new ModelAndView("/livroAdd");
		mv.addObject("livro", livro);
		
		return mv;
	}
	
	//Vai para tela de edição de livros (mesma tela de adição, contudo é enviado para a view um objeto que já existe)
	@GetMapping("/livro/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		
		return add(service.findOne(id));
	}
	
	//Exclui um livro por seu ID e redireciona para a tela principal
	@GetMapping("/livro/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		
		service.delete(id);
		
		return findAll();
	}
	
	//Recebe um objeto preenchido do Thymeleaf e valida 
	//Se tudo estiver ok, salva e volta para tela principal
	//Se houver erro, retorna para tela atual exibindo as mensagens de erro
	@PostMapping("/livro/save")
	public ModelAndView save(@Valid Livro livro, BindingResult result) {
		
		if(result.hasErrors()) {
			return add(livro);
		}
		
		service.save(livro);
		
		return findAll();
	}
	
}
