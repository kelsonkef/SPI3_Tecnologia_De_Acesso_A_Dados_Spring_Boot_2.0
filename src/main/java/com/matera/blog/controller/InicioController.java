package com.matera.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class InicioController {
	
	@GetMapping("/")
	public ModelAndView inicio() {
		ModelAndView mv = new ModelAndView("/inicio");
		return mv;
	}
}
